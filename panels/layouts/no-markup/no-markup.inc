<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('No markup'),
  'category' => t('storah'),
  'icon' => 'no-markup.png',
  'theme' => 'no_markup',
  'regions' => array('content' => t('Content')),
);
