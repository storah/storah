<?php

/**
* Implements theme_panels_default_style_render_region($vars)
*
* Remove panels seperator
*/
function storah_panels_default_style_render_region($vars) {
  $output = '';
  $output .= implode('', $vars['panes']);
  return $output;
}
