<?php

/**
 * Implements hook_css_alter().
 *
 * Makes it posible to exclude css files from the theme info file.
 */
function storah_css_alter(&$css) {
  $theme_path = drupal_get_path('theme', 'storah');
  $excludes = _storah_alter(storah_theme_get_info('exclude'), 'css');
  $css = array_diff_key($css, $excludes);
}

function _storah_alter($files, $type) {
  $output = array();

  foreach($files as $key => $value) {
    if (isset($files[$key][$type])) {
      foreach ($files[$key][$type] as $file => $name) {
        $output[$name] = FALSE;
      }
    }
  }
  return $output;
}
