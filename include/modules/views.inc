<?php

/**
 * @file
 * Provides views theme override functions
 */

/**
 * Display a view as a table style.
 */
function storah_preprocess_views_view_table(&$vars) {
  $vars['classes_array'][] = 'table';
}

function storah_preprocess_views_view_grid(&$vars) {
  $vars['class'] .= ' table';
}
