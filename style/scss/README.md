# Style
This style structure is base on SMACSS and Sass (SCSS).
New to SMACSS? Here is something to [read](http://smacss.com/) and [watch](http://tv.adobe.com/watch/adc-presents-smacss/smacss-introduction-to-a-scalable-and-modular-architecture-for-css/)  
New to Sass? Here is something to [read](http://sass-lang.com/) and [watch](http://net.tutsplus.com/tutorials/other/mastering-sass-lesson-1/)

Styles are broken down into the following groups:
* [Base](http://smacss.com/book/type-base)— These are defaults styles, usually for single element selectors.
* [Layout](http://smacss.com/book/type-layout)— Divides the page into sections, usually holding modules together.
* [Module](http://smacss.com/book/type-module)— Reusable, modular parts of the design: callouts, sidebar sections, product lists, and so on.
* [State](http://smacss.com/book/type-state)— Describes how the module or layout looks in a particular state, also in different page views.
* [Theme](http://smacss.com/book/type-theme)— Describes how modules or layouts might look.

