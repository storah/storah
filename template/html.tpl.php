<?php
/**
 * @file
 * Override html template file
 *
 */
?>

<!DOCTYPE html>
<!--[if IE 8]><html class="lt-ie10 ie8" lang="<?php print $language->language; ?>"><![endif]-->
<!--[if IE 9]><html class="lt-ie10 ie9" lang="<?php print $language->language; ?>"><![endif]-->
<!--[if gt IE 9]><!--><html lang="<?php print $language->language; ?>" ><!--<![endif]-->

  <head>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body <?php empty($classes) ? : print 'class="' . $classes . '"'; print $attributes; ?>>
    <?php print $page; ?>
  </body>
</html>
