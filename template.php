<?php

/**
 * include template overwrites
 */
$theme_path = drupal_get_path('theme', 'storah');
require_once $theme_path . '/include/storah.inc';
require_once $theme_path . '/include/css.inc';
require_once $theme_path . '/include/form.inc';
require_once $theme_path . '/include/panels.inc';

// Load module specific files in the modules directory.
$includes = file_scan_directory($theme_path . '/include/modules', '/\.inc$/');
foreach ($includes as $include) {
  if (module_exists($include->name)) {
    require_once $include->uri;
  }
}

/**
 * Implement template_preprocess_html()
 */
function storah_preprocess_html(&$vars) {
  // Define classes to remove
  $classes = array(
    'html',
    'not-front',
    'front',
    'logged-in',
    'not-logged-in',
    'two-sidebars',
    'one-sidebar sidebar-first',
    'one-sidebar sidebar-second',
    'no-sidebars',
    'toolbar',
    'toolbar-drawer'
  );

  // Remove cleasses from body
  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], $classes));
  $vars['classes_array'] = preg_grep('/^node-type/', $vars['classes_array'], PREG_GREP_INVERT);
  $vars['classes_array'] = preg_grep('/^page-node/', $vars['classes_array'], PREG_GREP_INVERT);
}
